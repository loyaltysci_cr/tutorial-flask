from flask import Flask
from flaskconfig import FlaskConfig
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


flask_server = Flask(__name__)
flask_server.config.from_object(FlaskConfig)
db = SQLAlchemy(flask_server)
migrate = Migrate(flask_server, db)

from app.api import api_bp
from app import routes
from app.models import user

flask_server.register_blueprint(api_bp, url_prefix='/api')