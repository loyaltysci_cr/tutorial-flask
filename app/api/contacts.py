from flask import jsonify
from app.api import api_bp
from app.api.responses import no_content, bad_request


@api_bp.route('/contacts/<int:user_id>')
def get_contacts(user_id):
    contacts = {
        1: [
            {
                'name': 'Pizza Express',
                'number': '+506 11223344'
            },
            {
                'name': 'Zapateria',
                'number': '+506 55667788'
            },
        ],
        2: [
            {
                'name': 'Burguer Express',
                'number': '+506 99112233'
            },
            {
                'name': 'Supermercado',
                'number': '+506 44556677'
            },
        ],
    }
    if user_id in contacts:
        return jsonify(contacts[user_id])
    else:
        return no_content()
