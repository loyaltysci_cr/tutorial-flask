from flask import jsonify
from werkzeug.http import HTTP_STATUS_CODES


def generic_response(status_code, message=None):
    payload = {'info': HTTP_STATUS_CODES.get(status_code, 'Unknown error')}
    if message:
        payload['message'] = message
    response = jsonify(payload)
    response.status_code = status_code
    return response


def bad_request(message):
    return generic_response(400, message)


def no_content():
    return generic_response(204)


