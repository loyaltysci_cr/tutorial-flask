from flask import jsonify
from app.api import api_bp


@api_bp.route('/users')
def get_users():
    users = [
        {
            'id': 1,
            'name': 'Jose Ruiz',
            'email': 'jruiz@loyaltysci.com'
        },
        {
            'id': 2,
            'name': 'Luis Pereira',
            'email': 'lpereira@mail.com'
        },
    ]
    return jsonify(users)
