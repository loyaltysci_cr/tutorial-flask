import os

basedir = os.path.abspath(os.path.dirname(__file__))


class FlaskConfig(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'secure-private-key'
    ITEMS_PER_PAGE = 5
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
                              'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
