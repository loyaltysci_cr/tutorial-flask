# Tutorial Flask

## Conexión a base datos

Para establecer conexión con una base de datos utilizaremos Flask SQLAlchemy  https://flask-sqlalchemy.palletsprojects.com/en/2.x/

    pip install flask-sqlalchemy

Y utilizaremos una herramienta de migración llamada Flask-Migrate (https://github.com/miguelgrinberg/flask-migrate)

    pip install flask-migrate

El primer paso es definir la ruta de la base de datos a utilizar en `flaskconfig.py`

```python
basedir = os.path.abspath(os.path.dirname(__file__))

class FlaskConfig(object):
    # ...
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
```

En `app/__init__.py` 

```python
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

db = SQLAlchemy(flask_server)
migrate = Migrate(flask_server, db)

```

Crearemos un nuevo paquete `app/models` donde definiremos los modelos de los datos que se almacenarán en la base de datos. 

En `app/models/user.py

```python
from app import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    email = db.Column(db.String(120), index=True, unique=True)

    def __repr__(self):
        return '<User {}>'.format(self.email)

```

Una vez definido y validado el modelo, procedemos a inicializar la base de datos

    flask db init

Generamos un el script de actualización de base de datos

    flask db migrate -m 'init_db'

Actualizamos la base de datos utilizando los scripts de migración

    flask db upgrade



Luego de definir el modelo, desde linea de comandos podemos interactuar con el modelo
```python
>>> from app.models.user import User
>>> u = User(username='susan', email='susan@example.com')
>>> u
<User susan>
```




## Ejercicio
Crear un nuevo módulo `contact` en el paquete `models`, utilizando la siguiente definición

```python
    {
        'name': 'Pizza Express',
        'number': '+506 11223344'
    }
```


```python
@flask_server.route('/usuarios', methods=['POST'])
def add_user():
    data = request.get_json() or {}
    if 'name' not in data or 'email' not in data:
        return 'debe indicar el nombre de usuario'
    if User.query.filter_by(email=data['email']).first():
        return 'utilice otro email'
    user = User()
    user.from_dict(data)
    db.session.add(user)
    db.session.commit()
    response = jsonify(user.to_dict())
    response.status_code = 201


def from_dict(self, data, new_user=False):
    for field in ['name', 'email']:
        if field in data:
            setattr(self, field, data[field])
    if new_user and 'password' in data:
        self.set_password(data['password'])

def to_dict(self):
    data = {
        'id': self.id,
        'name': self.name,
        'email': self.email,
    }
    return data
```