# Tutorial Flask
## Archivo de configuración

En la raíz del proyecto se creará un módulo llamado `flaskconfig.py`

```python
import os

class FlaskConfig(object):
    SECRET_KEY = 'secure-private-key'
    ITEMS_PER_PAGE = 5
```

Se debe indicar en la instancia de Flask, que debe utilizar cargar la configuracion definida en este objeto. En `app/__init__.py'

```python

from flaskconfig import FlaskConfig


flask_server.config.from_object(FlaskConfig)
```


Validaremos desde `flask shell` que la aplicación pueda los valores de configuración

    >> from app import flask_server
    >> flask_server.config['SECRET-KEY']

Se pueden definir valores que sean reemplazados por  valores en el ambiente

```python

import os

class FlaskConfig(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'secure-private-key'
    .
    .
    .

```

### Ejercicio
- Definir en la variable de ambiente `SECRET_KEY`
- Validar desde flask shell que la aplicación utilice el valor establecido en la variable de ambiente