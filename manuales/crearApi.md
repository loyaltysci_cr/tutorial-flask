Tutorial Flask
--------------------

## Crear endpoint para obtener usuarios y contactos

Dentro de `app` crear un paquete llamado `api`.

El  archivo `app/api/__init__.py`  debe contener lo siguiente:

```python
from flask import Blueprint

bp = Blueprint('api', __name__)
```

Crear los módulos `users.py` y `contacts.py` dentro del paquete api

Para usuarios se define la siguiente estructura

```python 
users = [
{
    'id': 1,
    'name': 'Jose Ruiz',
    'email': 'jruiz@loyaltysci.com'
},
{
    'id': 2,
    'name': 'Luis Pereira',
    'email': 'lpereira@mail.com'
},
]
```
y para contactos

```python 
contacts = {
    1: [
        {
            'name': 'Pizza Express',
            'number': '+506 11223344'
        },
        {
            'name': 'Zapateria',
            'number': '+506 55667788'
        },
    ],
    2: [
        {
            'name': 'Burguer Express',
            'number': '+506 99112233'
        },
        {
            'name': 'Supermercado',
            'number': '+506 44556677'
        },
    ],
}
```

Dentro del módulo `app/api/users.py` se definirá un endpoint para obtener una lista de usuarios

```python
from flask import jsonify
from app.api import api_bp


@api_bp.route('/users')
def get_users():
    users = [
        {
            'id': 1,
            'name': 'Jose Ruiz',
            'email': 'jruiz@loyaltysci.com'
        },
        {
            'id': 2,
            'name': 'Luis Pereira',
            'email': 'lpereira@mail.com'
        },
    ]
    return jsonify(users)
```

### Ejercicios
- Crear un api para obtener los contactos asociados a un usuario, recibiendo el id del usuario mediante URL
- Crear un api que retorne los datos de un usuario recibiendo el id mediante la URL


