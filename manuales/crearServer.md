
# Tutorial Flask
## Crear Servidor Flask

    pip install flask

Desde una terminal de python se puede ejecutar el siguiente comando para validar que Flask se ha instalado correctamente

```python 
import flask
```
o directamente desde terminal

```bash
python -c 'import flask' 
```
Dentro del proyecto, crear un nuevo paquete llamado ` app` . En el archivo ` app/__ini__.py` se definirá nuestra instancia de Flask.

```python
from flask import Flask
flask_server = Flask(__name__)
```

En el codigo anterior se crea una instancia de la clase `Flask`. La variable `__name__`  es el nombre del módulo o paquete de la aplicación en donde la clase buscará los respectivos templates, archivos estáticos, entre otros.

Para validar que hemos creado la instancia de Flask correctamente, procederemos a ejecutar el servidor desde terminal. Para esto es necesario definir el punto de entrada a nuestra aplicación, el cual será el archivo `contactos.py` en la carpeta principal del proyecto. Dicho archivo tendrá un import a nuestra instancia de Flask

```python
from app import flask_server
```

Para iniciar la aplicación se necesitan ejecutar las siguientes instrucciones en la terminal

    set FLASK_APP=contactos.py
    set FLASK_ENV=development
    flask run

Luego de ejecutar las instrucciones anteriores, la aplicación iniciará e imprimirá en la terminal el output del servidor. 

El puerto 5000 es el puerto por defecto utilizado por Flask. Si accedemos a `localhost:5000` desde el navegador  podremos acceder a la ruta principal (index) de la aplicación.

Dado que no hemos definido ninguna ruta en nustra aplicación, el navegador nos mostrará un mensaje indicando que no ha podido encontrar la URL consultada.

En `app/routes.py` definiremos la ruta para la pagina principal de nuestra aplicación.

```python
from app import flask_server


@flask_server.route('/')
def index():
    return "Hola Mundo!"
```

En la documentación de Flask podremos encontrár más informacion de las capacidades del decorator route (http://flask.pocoo.org/docs/1.0/quickstart/#routing)

Como ultimo paso, debemos importar el módulo de rutas en `app/__init__py`

```python
from app import routes
```

## Variables de ambiente persistentes

Cada vez que se abre una nueva terminal se deben inicializar las variables de ambientes , dado que son visibles unicamente en la sesión en la que se definieron.  Una forma de que estas variables sean "persistentes" es utilizando `python-dotenv`. 

    pip install python-dotenv

Crearemos un nuevo archivo llamado `.flaskenv` en la raíz de nuestro proyecto. En este nuevo archivo se definirán las variables de ambiente que se inicializarán para el proyecto en distintas terminales.
```python
FLASK_APP=perfiles.py
```
