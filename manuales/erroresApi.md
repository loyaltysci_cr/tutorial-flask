
## Manejo de errores a nivel de API
Ingresar a la siguiente url
http://localhost:5000/api/contacts/99

Veremos un Key Error. Para manejar este tipo de errores se crearán respuestas genericas para errores.

Crear `errors.py` en `app/api/`

```python
from flask import jsonify
from werkzeug.http import HTTP_STATUS_CODES


def generic_response(status_code, message=None):
    payload = {'info': HTTP_STATUS_CODES.get(status_code, 'Unknown error')}
    if message:
        payload['message'] = message
    response = jsonify(payload)
    response.status_code = status_code
    return response


def bad_request(message):
    return generic_response(400, message)


def no_content():
    return generic_response(204)

```

### Ejercicio
- Validar existencia en el diccionario de contactos del id de usuario consultado al endpoint `/contacts`  para evitar el `Key Error` cuando el valor no exista.
-  Validar existencia en el diccionario de usuarios el id de usuario consultado al endpoint `/users`  para evitar el `Key Error` cuando el valor no exista.