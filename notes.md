
## Crear modelo de datos
Dentro de `app/` crear un nuevo paquete en el cual se crearan las clases para los objetos que utilizaremos en nuestro api. Se crearán las clases Usuario y Contacto



[Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet "Markdown Cheatsheet")

-----------
-----------
-----------
-----------
-----------

    methods=['GET']
-----------------------
[I'm a reference-style link][Arbitrary case-insensitive reference text]

[I'm a relative reference to a repository file](../blob/master/LICENSE)


Inline `code` has `back-ticks around` it.

```python



config.py
import os

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'


app/__init__.py
from flask import Flask
from config import Config

app = Flask(__name__)
app.config.from_object(Config)

from app import routes
```

```bash
flask run
```